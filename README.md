Dependencies:
```
sudo apt install apt-file reprepro curl devscripts
```

The app-install-data package provides application data for use with [trisquel-app-install](https://devel.trisquel.info/chaosmonk/gnome-app-install).  Generate the source for app-install-data for Trisquel release `$RELEASE` with `./make_source.sh $RELEASE $TRISQUEL_VERSION`.  For example,
```
./make_source.sh etiona 9.0
```
creates the source for app-install-data in `source_etiona`.

This package should be rebuilt for each new Trisquel release, and whenever applications are added/removed from Trisquel's repositories.
